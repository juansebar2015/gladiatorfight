//
//  Character.swift
//  Gladiator_Fight
//
//  Created by Juan Ramirez on 4/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Character {
    
    private var _hp: Int
    private var _attackPwr: Int
    private var MAX_DEFENCE = 15
    //private var _name: String
    
    var hp: Int {
        get{
            return _hp
        }
    }
    
    var attackPwr: Int {
        get{
            return _attackPwr
        }
    }
    
    var isAlive: Bool {
        if _hp <= 0 {
            return false
        } else {
            return true
        }
    }
    
    init(hp: Int, attackPwr: Int){
        _hp = hp
        _attackPwr = attackPwr
    }
    
    
    //Attack function
    func attemptAttack(attackValue: Int) -> Bool {
        
        if attackValue >= MAX_DEFENCE {
            //Attack
            _hp -= attackValue
            
            if _hp < 0 {
                _hp = 0
            }
            
            return true
            
        } else{
            //Attack Blocked
            return false
        }
        
        
        
    }
    
    func generateAttack() -> Int {
        
        return Int(arc4random_uniform(UInt32(_attackPwr)))
    }
    
}
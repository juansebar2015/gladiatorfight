//
//  Spartan.swift
//  Gladiator_Fight
//
//  Created by Juan Ramirez on 4/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Spartan: Character {

    
    
    private var _name = "Spartan"
    
    var name: String {
        get{
            return _name
        }
    }
    
    convenience init(name: String, hp: Int, attackPwr: Int){
        
        self.init(hp: hp, attackPwr: attackPwr)
        
        _name = name
        
        
    }
    
}
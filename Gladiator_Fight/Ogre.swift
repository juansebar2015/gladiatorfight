//
//  Ogre.swift
//  Gladiator_Fight
//
//  Created by Juan Ramirez on 4/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Ogre: Character {
    
    private var _name = "Ogre"
    
    var name: String {
        get{
            return _name
        }
    }
    
    convenience init(name: String, hp: Int, attackPwr: Int) {
        
        self.init(hp: hp, attackPwr: attackPwr)
        _name = name
        
    }
    
    
}
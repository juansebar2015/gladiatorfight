//
//  ViewController.swift
//  Gladiator_Fight
//
//  Created by Juan Ramirez on 4/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var playerOneImage: UIImageView!
    @IBOutlet weak var playerTwoImage: UIImageView!
    
    @IBOutlet weak var playerOneHp: UILabel!
    @IBOutlet weak var playerTwoHp: UILabel!
    @IBOutlet weak var outputLabel: UILabel!
    
    @IBOutlet weak var restartMatchLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    var spartan: Spartan!
    var ogre: Ogre!
    
    var backgroundMusic: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restartMatchLabel.hidden = true
        yesButton.hidden = true
        noButton.hidden = true
        
        //Setup background music
        var path = NSBundle.mainBundle().pathForResource("bensound-epic", ofType: "mp3")
        var soundURL = NSURL(fileURLWithPath: path!)
        
        do{
            try backgroundMusic = AVAudioPlayer(contentsOfURL: soundURL)
            backgroundMusic.play()
            
        } catch let error as NSError {
            print(error.debugDescription)
        }
        
        spartan = Spartan(name: "Leonidas", hp: 100, attackPwr: 25)
        ogre = Ogre(name: "Okra", hp: 100, attackPwr: 25)
        
        playerOneImage.hidden = false
        playerTwoImage.hidden = false

    }

    

    @IBAction func playerOneAttack(sender: AnyObject) {
     
        let attack = ogre.generateAttack()
        
        //Attempt Attack on Spartan
        if spartan.attemptAttack( attack ){
            
            //Update HP
            playerTwoHp.text = "\(spartan.hp) HP"
            successfulAttack(ogre.name, defender: spartan.name, attackValue: attack)
            
            //outputLabel.text = "\(ogre.name) hits \(attack) on \(spartan.name)"
            
        } else {
            //Attack missed
            blockedAttack(ogre.name, defender: spartan.name)
            
            //outputLabel.text = "\(spartan.name) blocked \(ogre.name)'s attack!"
        }
        
//        if !attack(ogre, defender: spartan){
//            
//        }
        
        if !spartan.isAlive {
            playerTwoImage.hidden = true
            
            outputLabel.text = "\(ogre.name) Wins!!!"
            
            // Reset Match
            resetMatch()
        }
        
    }

    @IBAction func playerTwoAttack(sender: AnyObject) {
        
        let attack = spartan.generateAttack()
        
        //Attempt attack on Ogre
        if ogre.attemptAttack( attack ){
            
            //Update HP
            playerOneHp.text = "\(ogre.hp) HP"
            
            successfulAttack(spartan.name, defender: ogre.name, attackValue: attack)
            //outputLabel.text = "\(spartan.name) hits \(attack) on \(ogre.name)"
            
        }else{
            //Attack missed
            blockedAttack(spartan.name, defender: ogre.name)
            
            //outputLabel.text = "\(ogre.name) blocked \(spartan.name)'s attack!"
        }
        
//       attack(spartan, defender: ogre)
        
        if !ogre.isAlive {
            playerOneImage.hidden = true
            
            outputLabel.text = "\(spartan.name) Wins!!!"
            
            // Reset Match
            resetMatch()
        }
        
    }
    
    func successfulAttack(attacker: String, defender: String, attackValue: Int){
        outputLabel.textColor = UIColor.redColor()
        outputLabel.text = "\(attacker) hits \(attackValue) on \(defender)"
    }
    
    func blockedAttack(attacker: String, defender: String){
        outputLabel.textColor = UIColor.blueColor()
        outputLabel.text = "\(defender) blocked \(attacker)'s attack!"
    }
    
    func resetMatch() {
        restartMatchLabel.hidden = false
        yesButton.hidden = false
        noButton.hidden = false
    }
    
    @IBAction func onYesPress(sender: AnyObject) {
        viewDidLoad()
    }
    
    @IBAction func onNoPress(sender: AnyObject) {
        //Go back to main menu
    }
//    func attack(attacker: Character, defender: Character) -> Bool {
//        //Attempt attack on Ogre
//        if defender.attemptAttack( attacker.generateAttack() ){
//            
//            //Update HP
//            playerOneHp.text = "\(ogre.hp) HP"
//            
//        }else{
//            //Attack missed
//        }
//        
//        return defender.isAlive
//    }
}

